#! /bin/sh
# this script is made by the A_rose team
# to make the installation of A_rose easier
# this will help install base packages
# as well as setup configuration files
# first time use should be as easy as
# running this script and entering the
# password once
# Author Carlos Gudino
# 22-12-21
# 22-03-17 (edit)

# installs zsh 
sudo pacman -S zsh --noconfirm

# (remove soon) installing omz
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# we are still in tty
/bin/zsh -i -c "omz theme set jonathan;echo 'type exit: ... to exit!';sleep 5"
# sorry you have to manually exit
# in order to continue

# openssh install
sudo pacman -S openssh --noconfirm;sudo systemctl enable sshd; sudo systemctl start sshd
echo "ssh is enabled"; sleep 4

# basic stuff
sudo pacman -S base-devel xf86-video-intel xorg xorg-xinit nitrogen picom --noconfirm

# AUR
git clone https://aur.archlinux.org/yay-git.git
cd yay-git
makepkg -si
cd ~

# adding packages from AUR
yay -S nerd-fonts-mononoki brave-bin shell-color-scripts ulauncher --noconfirm

# adding rose-repo
echo "" | sudo tee -a /etc/pacman.conf
echo "[rose-repo]" | sudo tee -a /etc/pacman.conf
echo "SigLevel = Optional DatabaseOptional" | sudo tee -a /etc/pacman.conf
echo "Server = https://gitlab.com/flym3t0mars/\$repo/-/raw/main/\$arch" | sudo tee -a /etc/pacman.conf
sudo pacman -Syyu --noconfirm

# the main ingredients
sudo pacman -S dwm-rose dwmblocks-rose xdg-user-dirs alsa-utils thunar thunar-volman gvfs lxappearance conky feh mpv flameshot ranger neofetch --noconfirm


cp /etc/X11/xinit/xinitrc $HOME/.xinitrc

# removing last 5 lines
count=5
for i in $(seq $count); do
        sed -i '$ d' .xinitrc
done

echo "nitrogen --restore" >> $HOME/.xinitrc
echo "picom &" >> $HOME/.xinitrc
echo "dwmblocks &" >> $HOME/.xinitrc
echo "conky -c $HOME/.config/conky/.conkyrc" >> $HOME/.xinitrc
echo "exec dwm" >> $HOME/.xinitrc

# conky dependency
git clone 

# getting wallpapers
mkdir Pictures; cd Pictures; git clone https://gitlab.com/flym3t0mars/wallpapers.git; cd $HOME

# adding lines to .zshrc
echo "colorscript random" >> $HOME/.zshrc
echo "if [-f ~./zsh_aliases ]; then" >> $HOME/.zshrc
echo "    . ~/.zsh_aliases" >> $HOME/.zshrc
echo "fi" >> $HOME/.zshrc
echo "" >> $HOME/.zshrc
echo "[[ $(fgconsole 2>/dev/null) == 1 ]] && exec startx --vt1" >> $HOME/.zshrc


# adding aliases
touch .zsh_aliases
echo "alias clear='clear; colorscript random'" >> .zsh_aliases
echo "alias cls='clear'" >> .zsh_aliases
echo "alias ccls='cd ~; cls'" >> .zsh_aliases

sudo pacman -S python python-pip python-virtualenv --noconfirm
sudo pip install pillow



######## Seasonal ########

initializeANSI()
{
  esc="^["

  blackf="${esc}[30m";   redf="${esc}[31m";    greenf="${esc}[32m"
  yellowf="${esc}[33m"   bluef="${esc}[34m";   purplef="${esc}[35m"
  cyanf="${esc}[36m";    whitef="${esc}[37m"

  blackfbright="${esc}[90m";   redfbright="${esc}[91m";    greenfbright="${esc}[92m"
  yellowfbright="${esc}[93m"   bluefbright="${esc}[94m";   purplefbright="${esc}[95m"
  cyanfbright="${esc}[96m";    whitefbright="${esc}[97m"

  blackb="${esc}[40m";   redb="${esc}[41m";    greenb="${esc}[42m"
  yellowb="${esc}[43m"   blueb="${esc}[44m";   purpleb="${esc}[45m"
  cyanb="${esc}[46m";    whiteb="${esc}[47m"

  boldon="${esc}[1m";    boldoff="${esc}[22m"
  italicson="${esc}[3m"; italicsoff="${esc}[23m"
  ulon="${esc}[4m";      uloff="${esc}[24m"
  invon="${esc}[7m";     invoff="${esc}[27m"

  reset="${esc}[0m"
}

# note in this first use that switching colors doesn't require a reset
# first - the new color overrides the old one.
# ****************************** Building blocks:  ^v^h  ^v^s  ^v^r  ^v^q  ^v^d  ^v^`  ^v^p  ^v^l  ^w^o   ^u^p  ^u^q  ^>

initializeANSI

cat << EOF

${purplef}         ^v^d          ^v^d      ^v^d          ^v^d
${redf}     ^v^d ^v^h ^v^h ^v^h ^v^h ^v^d     ^v^d ^v^h ^v^h ^v^h ^v^h ^v^d    ^v^d ^v^h ^v^h ^v^h ^v^h ^v^d     ^v^d ^>
${purplef}    ^v^h ^v^h ^v^` ^v^` ^v^` ^v^` ^v^h ^v^h   ^v^h ^v^h ^v^` ^v^` ^v^` ^v^` ^v^h ^v^h  ^v^h ^v^h ^v^` ^v^` ^v>
${greenf}   ^v^h   ^v^` ^v^d ^v^`   ^v^h  ^v^h   ^v^` ^v^d ^v^`   ^v^h  ^v^h   ^v^` ^v^d ^v^`   ^v^h  ^v^h   ^v^` ^v^d >
${yellowy}   ^v^h ^v  ^v^d ^v  ^v  ^v^d ^v  ^v  ^v^h  ^v^h ^v  ^v^d ^v  ^v  ^v^d ^v  ^v  ^v^h  ^v^h ^v  ^v^d ^v  ^v  ^v>
${redf}M${purplef}E${redf}R${purplef}R${redf}Y
${redf}C${purplef}H${redf}R${purplef}I${redf}S${purplef}T${redf}M${purplef}A${redf}S
${reset}

EOF

sleep 10;exit
######## Seasonal ########





